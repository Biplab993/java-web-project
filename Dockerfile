FROM tomcat:alpine
 
COPY ./target/java-web-project.war ./usr/local/tomcat/webapps/

EXPOSE 8080
ENTRYPOINT ["catalina.sh", "run"]